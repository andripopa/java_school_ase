package org.ase.javaschool.beer.service.api;

import org.ase.javaschool.beer.domain.Beer;
import org.ase.javaschool.beer.domain.Consumer;

/**
 * Drinking service class .
 * @author andrei
 *
 */
public interface DrinkingService {
	void requestCheck();
	void orderBeer(Consumer consumer, Beer beer);
	int getBeersOrdered();
}
