package org.ase.javaschool.beer.service;

import org.ase.javaschool.beer.domain.Beer;
import org.ase.javaschool.beer.domain.Consumer;
import org.ase.javaschool.beer.service.api.DrinkingService;

/**
 * Basic implementation for {@link DrinkingService}.
 * 
 * @author andrei
 */
public class DrinkingServiceImpl implements DrinkingService {

	private Consumer[] consumers;
	private Beer[] beers;
	private int beersOrdered;
	

	public DrinkingServiceImpl(Consumer[] consumers, Beer[] beers) {
		this.consumers = consumers;
		this.beers = beers;
	}

	@Override
	public void orderBeer(Consumer consumer, Beer beer) {
		consumer.setBeerCount(consumer.getBeerCount() + 1);
		beersOrdered++;
	}

	@Override
	public void requestCheck() {
		System.out.println("Check Please!");
	}
	
	@Override
	public int getBeersOrdered() {
		return beersOrdered;
	}

}
