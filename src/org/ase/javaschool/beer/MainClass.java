package org.ase.javaschool.beer;

import org.ase.javaschool.beer.domain.Beer;
import org.ase.javaschool.beer.domain.Consumer;
import org.ase.javaschool.beer.service.DrinkingServiceImpl;
import org.ase.javaschool.beer.service.api.DrinkingService;

public class MainClass {

	public static void main(String[] args) {
		Consumer[] friends = new Consumer[4];
		Beer[] beers = new Beer[5];

		friends[0] = new Consumer("Gigi", "Ionescu", 0.5);
		friends[1] = new Consumer("Viorel", "Popescu", 0.15);
		friends[2] = new Consumer("Getonel", "Popa", 0.75);
		friends[3] = new Consumer("Haralambie", "Constantinescu", 1.5);

		beers[0] = new Beer("Blondă", "Pilsner Urquell", 0.45, 10.0);
		beers[1] = new Beer("Neagră", "Kriek", 0.7, 16.0);
		beers[2] = new Beer("Albă", "Paulaner", 0.45, 12.0);
		beers[3] = new Beer("Arămie", "Staropramen", 0.45, 6.0);
		beers[4] = new Beer("Lager", "Spitfire", 0.5, 18.0);

		DrinkingService drinkingService = new DrinkingServiceImpl(friends, beers);

		drinkingService.orderBeer(friends[0], beers[0]);
		drinkingService.orderBeer(friends[1], beers[3]);
		drinkingService.orderBeer(friends[2], beers[4]);
		System.out.println("Până acum s-au comandat " + drinkingService.getBeersOrdered() + " beri");
		drinkingService.orderBeer(friends[3], beers[2]);
		System.out.println("Până acum s-au comandat " + drinkingService.getBeersOrdered() + " beri");

	}
}