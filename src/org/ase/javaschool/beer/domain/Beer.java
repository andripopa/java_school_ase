package org.ase.javaschool.beer.domain;

public class Beer {

	private String type;
	private String name;
	private double alcooholContent;
	private double price;

	public Beer() {

	}

	public Beer(String type, String name, double alcooholContent, double price) {
		this.type = type;
		this.name = name;
		this.alcooholContent = alcooholContent;
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAlcooholContent() {
		return alcooholContent;
	}

	public void setAlcooholContent(double alcooholContent) {
		this.alcooholContent = alcooholContent;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
