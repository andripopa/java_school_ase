package org.ase.javaschool.beer.domain;

public class Consumer {
	private String firstName;
	private String lastName;
	private double sobrietyThreshold;
	private int beerCount;

	public Consumer() {

	}

	public Consumer(String firstName, String lastName, double sobrietyThreshold) {
		this.firstName 	= firstName;
		this.lastName 	= lastName;
		this.sobrietyThreshold = sobrietyThreshold;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSobrietyThreshold() {
		return sobrietyThreshold;
	}

	public void setSobrietyThreshold(double sobrietyThreshold) {
		this.sobrietyThreshold = sobrietyThreshold;
	}

	public int getBeerCount() {
		return this.beerCount;
	}

	public void setBeerCount(int beerCount) {
		this.beerCount = beerCount;
	}
}
